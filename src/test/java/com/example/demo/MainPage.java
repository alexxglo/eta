package com.example.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class MainPage {

  public static void main(String[] args) throws IOException {
    Properties prop = setup();
    System.setProperty("webdriver.chrome.driver", prop.getProperty("chromedriver.location"));

    ChromeOptions options = new ChromeOptions();
    options.addArguments("--no-sandbox");
    options.addArguments("--disable-dev-shm-usage");

    // declaration and instantiation of objects/variables

    WebDriver driver = new ChromeDriver();

    // Launch website
    driver.navigate().to(prop.getProperty("page.url"));

    // Click on the search text box and send value
    driver.findElement(By.xpath(prop.getProperty("signin.button"))).click();

  }

  private static Properties setup() throws IOException {
    Properties prop = new Properties();
    InputStream input = new FileInputStream("src/test/resources/application.properties");
    prop.load(input);
    return prop;
  }
}
