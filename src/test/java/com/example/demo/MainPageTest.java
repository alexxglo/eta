//package com.example.demo;
//
//import com.codeborne.selenide.Configuration;
//import com.codeborne.selenide.Selenide;
//import com.codeborne.selenide.logevents.SelenideLogger;
//import io.qameta.allure.selenide.AllureSelenide;
//import org.junit.jupiter.api.*;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//import static com.codeborne.selenide.Condition.attribute;
//import static com.codeborne.selenide.Condition.visible;
//import static com.codeborne.selenide.Selenide.*;
//
//public class MainPageTest {
//  MainPage mainPage = new MainPage();
//
//  @BeforeAll
//  public static void setUpAll() {
//    Configuration.browserSize = "1280x800";
//    SelenideLogger.addListener("allure", new AllureSelenide());
//  }
//
//  @BeforeEach
//  public void setUp() {
//    open("https://test.mijn.ing.nl/login?profileId=9f7c0dbf-7fe1-4c28-9a5a-ccc133030568&loa=5&personId=9d1d6bf0-4bdc-4337-a480-83b88b51225e&redirectUrl=/banking");
//  }
//
//  @Test
//  public void search() {
//
//    $("[data-test='search-input']").sendKeys("Selenium");
//    $("button[data-test='full-search-button']").click();
//
//    $("input[data-test='search-input']").shouldHave(attribute("value", "Selenium"));
//  }
//
//  @Test
//  public void toolsMenu() {
//    $("div[data-test='menu-main-popup-content']").shouldBe(visible);
//  }
//
//  @Test
//  public void navigationToAllTools() {
//    $("#products-page").shouldBe(visible);
//
//    assertEquals("All Developer Tools and Products by JetBrains", Selenide.title());
//  }
//}
